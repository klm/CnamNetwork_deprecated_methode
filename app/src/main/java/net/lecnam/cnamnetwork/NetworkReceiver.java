package net.lecnam.cnamnetwork;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;
import android.widget.Toast;

public class NetworkReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        // TODO Auto-generated method stub
        Log.d("operateChangedState","NetworkReceiver onReceive appelé");
        MainActivity.getIns().operateChangedState(null);
        Intent callingIntent = new Intent(context, MainActivity.class);
        callingIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        callingIntent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
        context.startActivity(callingIntent);
    }

}
